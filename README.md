# Aide et ressources de software_name pour Synchrotron SOLEIL

## Résumé

- visualisation, analyse, normalisation, filtre, assemblage des données
- Open source

## Sources

- Code source: https://github.com/kuadrat/arpys
- Documentation officielle: https://arpys.readthedocs.io/en/latest/

## Navigation rapide

| Tutoriaux |
| - |
| [Tutoriel d'installation officiel](https://arpys.readthedocs.io/en/latest/installing.html) |
| [Tutoriaux officiels](https://arpys.readthedocs.io/en/latest/gap_analysis.html) |

## Installation

- Systèmes d'exploitation supportés: Windows,  MacOS,  python
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: text
- en sortie: png,  images
- sur un disque dur
